
// Lab 2
// Mike Murray

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


enum Rank
{
	Two = 2, // set it to two so it doesn't start out at a 0
	Three = 3,
	Four,
	Five,
	Six, 
	Seven, 
	Eight, 
	Nine, 
	Ten, Jack, 
	Queen, 
	King, 
	Ace
};

enum Suit{Club, Spade, Diamond, Heart};

struct Card
{ 
	Rank Rank; 
	Suit Suit;
};

int main()
{

	_getch();
	return 0;
}
